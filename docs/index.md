#Proyecto rediseño *casinobarcelona.es*

## Índice
- [Introducción](#introducción)
- [Objetivos](#objetivos)
- [Metodología de desarrollo](#metodología-de-desarrollo)
- [Negocio](#negocio)
- [SEO](#seo)
- [Detalle de secciones](#detalle-de-secciones)
    + [Headers y footer](#headers-y-footer)
    + [Home](#home)
      - [Slider](#slider)
      - [Widget de favoritos y recientes](#widget-de-favoritos-y-recientes)
      - [Seccion promocional](#seccion-promocional)
      - [Lobbies](#lobbies)
      - [Jackpots](#jackpots)
      - [Póker](#póker)
      - [Apuestas](#apuestas)
      - [Ruleta en vivo y juegos de mesa](#ruleta-en-vivo-y-juegos-de-mesa)
  * [Slots](#slots)
    + [Principal](#principal)
    + [Todas las slots](#todas-las-slots)
  * [Página de juego](#página-de-juego)
- [Casino](#casino)
- [Ruleta en vivo](#ruleta-en-vivo)
- [Promociones](#promociones)
- [Página de promoción](#página-de-promoci-n)
- [Poker](#poker)
- [Apuestas deportivas](#apuestas-deportivas)

## Introducción
Casino Barcelona Online es un portal de juego online apoyado por la marca Casino Barcelona, un casino físico ubicado en el paseo marítimo de Barcelona y miembro del grupo empresarial Grup Peralada.

En este portal los usuarios pueden depositar dinero real para usarlo en juegos que pueden retornar premios. Así mismo, los usuarios pueden retirar el dinero que tengan en su balance personal.

## Objetivos
Casino Barcelona Online nos encarga la tarea de rediseñar y optimizar su portal de juego online.

El contenido se gestiona a través de un CMS desarrollado en Java de código propietario. Este gestor de contenidos se llama Alira y está desarrollado por Tecnalis. Es un CMS dedicado a casinos online y usado por varios operadores de juego de España y Latinoamérica.

Este gestor de contenidos es muy básico a nivel de frontend y no permite usar herramientas de desarrollo como IDEs, Git, Webpack, etc. Todo se hace a través de su portal al que accedemos desde el navegador.

Esto ocasiona 2 problemas principales: un pésimo rendimiento y una alta dificultad para mantener un código limpio y ordenado.

## Metodología de desarrollo

Por las limitaciones técnicas descritas en el punto anterior hemos decidido prescindir de la plataforma todo lo posible, para ello queremos desarrollar el front con un proyecto de HTML / CSS y JS que se compile en local y se despliegue en un hosting externo y fiable como podría ser Netlify. Una vez subidos los archivos a Netlify, llamamos a los assets directamente desde la plataforma al hosting, por ejemplo: `<link rel="stylesheet" href="https://cbo-assets.netlify.app/index.f7231f56.css">`

Sería óptimo dividir el CSS en 2 archivos, uno el que contenga la estructura y que cargue en el head de forma bloqueante y otro con el resto del código que no sea bloqueante. Esta parte se puede obviar si se considera que añade una notable capa de complejidad al proyecto.

De este modo, cuando hacemos un deploy del código, automaticamente lo tenemos actualizado en el portal y además, forzamos al desarrollador a aplicar cambios siguiendo el estilo del proyecto en lugar de añadir bloques de CSS y JS en los archivos existentes por miedo de borrar algo que se use en otras partes del sitio.

## Negocio
El negocio principal del portal son los videojuegos online de apuestas con dinero real.

Los juegos no los desarrolla Casino Barcelona, sino que son de varios proveedores (desarrolladores) de juegos distintos, estos se integran en la plataforma a través de API.

Los juegos están divididos en 5 categorías:
- **Slots**
    - Comúnmente llamadas tragaperras. Es el negocio principal de CBO, es el portal de juego online con mayor número de slots de España, actualmente >1600.
- **Casino**
    - También llamados juegos de mesa
    - Juegos de 1 sólo jugador (jugador contra la máquina) de póker y blackjack
- **Apuestas virtuales**
    - Simuladores de partidos de futbol, carreras de coches y carreras de caballos en los que el jugador puede apostar
- **Ruleta en vivo**
    - Ruletas de casino que transmiten en directo, algunas son sólo una ruleta actuando automáticamente, otras son con un croupier que interactúa con los jugadores que escriben en el chat.
- **Poker**
    - Aplicación instalable en móviles y en PC Windows para jugar al póker con otros jugadores reales. También dispone de versión para navegador, pero se abre en una ventana nueva independiente de la web del Casino.

A nivel técnico y conceptual, las slots, los juegos de casino, las apuestas virtuales y las ruletas en vivo son lo mismo por lo que los listados de éstos y sus páginas de juego son iguales o muy parecidas entre ellas.

También existe un apartado de apuestas deportivas pero usa un sistema completamente independiente al que sólo le podemos cambier colores y fuentes de ciertos elementos. No hay que tenerlo en cuenta para este proyecto.

**El principal objetivo es llamar al usuario a pulsar sobre los botones de depositar**, siendo más importante incluso que llamarle a jugar.

## SEO
Toda la estructura de headings estará exclusivamente en los bloques de texto del final de cada página. Entonces, los titulos del contenido principal con serán realmente `<h1>` sinó por ejemplo `<div class="h1">`.

## Detalle de secciones
El diseño de este portal está en las fases finales de desarrollo. Pueden cambiar pequeños aspectos de éste pero no habrá cambios de estructura. Al apartado de usuario usuario le faltan los diseños para la vista desktop que se desarrollarán mientras se maqueta la parte del portal de juego.

El diseño puede verse en [este proyecto de Adobe XD](https://xd.adobe.com/view/821f8a98-6649-4518-b5f5-31499e5c4fac-d74c/) que repasaremos a continuación.

> **Importante:** Para optimizar el rendimiento de la página, deberemos cargar sólo el CSS y JS necesario para cada página. No siempre será posible ser tan granular, por ejemplo, en las páginas de juego de Slot y de Casino, el contenido no es igual pero se puede cargar todo el contenido de ambos en los mismos archivos, pero no cargaremos el CSS del detalle de promoción.
En todas las páginas habrá código común (header, footer, estructura, variables, helpers, etc...) y código específico de la página. 
El siguiente listado es una buena referencia para ver como separar los recursos.

#### Headers y footer
**Páginas 1 a 4**
El header contiene el menú principal con las 5 verticales y un menú secundario enlazando a la página de promociones.

En vista móvil el menú secundario se añade al principal y se despliega desde la izquierda de la pantalla tal y como se muestra en la página 22 del documento de XD.

Si el usuario no está logueado, el icono de usuario de la derecha le llevará directamente a la página de login.

Si está logueado, mostramos el balance del usuario, el botón de Depositar, su nombre, el tiempo de sesión y el icono para abrir el menú de usuario.

Pulsando sobre el balance se abrirá el widget de la página 23.

Pulsando sobre el icono de usuario, se abrirá desde la derecha el menu de usuario mostrado en la página 9.

En vista mobile, los elementos Depositar, balance, nombre y tiempo de sesión se muestran en un footer fijo.

En todas las páginas del portal de juego (no así en la parte usuario), se mostrará un footer con enlaces a secciones de la web, enlaces a redes sociales, iconos obligatorios por regulación de juego, metodos de pago, etc.

#### Home
**Páginas 8 y 43**
Las secciones de la home son:

##### Slider
Un slider con textos e imagenes, debemos buscar un script lo más ligero posible que nos permita convertir árboles de HTML complejos en slides.

##### Widget de favoritos y recientes
Estos widgets deben poderse plegar y desplegar, guardando el estado en una cookie para que a la siguiente recarga se mantengan en el mismo estado. La plataforma (Alira) no nos permite guardar preferencias del usuario por lo que estas opciones sólo podemos guardarlas a nivel de front.

Puede haber entre 0 y 8 elementos dentro de cada widget.

##### Seccion promocional
Iconos con puntos fuertes del negocio, título y eslógan.

##### Lobbies
Los lobbies son grupos de juegos. En el ejemplo mostramos lobbies de uegos recomendados y juegos nuevos.

Estos lobbies deben tener un scroll horizontal que en desktop se manejaran con botones para desplazar a izquierda y derecha y en mobile con gestos de swipe horizontal.

Se indicará al usuario que hay contenido oculto por el scroll mostrando una sombra a la izquierda y/o a la derecha del lobby segun la posición del scroll.

Los juegos del lobby tendrán un estado activo que se activará en el hover en desktop y tras hacer un tap en mobile. El estado activo mostrará el título, el CTA para jugar, un link a la demo y si está logueado el icono para añadir a favoritos.

Para la maquetación del botón de favoritos necesitaremos simular un fetch llamando a una función asíncrona que al responder cambiará el estado del icono del corazón. No hace falta que se guarde el estado del corazón entre refrescos de página.

Algunos juegos tienen premios acumulados. El premio acumulado se muestra como un número que aumenta su valor automaticamente randomizando unos céntimos varias veces por segundo.

Los juegos destacados, en desktop, ocupan el deble de alto y de ancho.

##### Jackpots
Esta sección muestra un acumulativo del valor de todos los jackpots, este valor tambien se muestra incrementándose.

Se muestran una selección de juegos con jackpots con un diseño diferente para llamar la atención. Este diseño de lobby sólo está presente en esta página y en la página de Slots. Tiene también scroll horizontal.

##### Póker
Sección de contenido estático.

##### Apuestas
En el diseño se muestra erróneamente el título "Novedades", debe ser "Apuestas".

Esta sección se carga de manera asíncrona a través de una API. Si es posible en el proceso de maquetación sería bueno simularlo pero no imprescindible.

Los botones no llevaran a ninguna parte en la maquetacion.

##### Ruleta en vivo y juegos de mesa
Sección de contenido estático.

### Slots
**Páginas 45 a 48**
En la página actual de slots tenemos el problema que, por limitaciones de la plataforma, tenemos que cargar las 1600 slots. Esto supone problemas de rendimiento tanto para los motores de búsqueda como para los navegadores de los usuarios.

En este proyecto vamos a dividir esta página en dos. La *página principal* de slots que mostrará sólo varios lobbies con selecciones de juegos y la página de *Todas las Slots* que mostrará el listado completo con un buscador avanzado. Así el grueso de usuarios aterrizará en la página con menos slots y solo los usuarios avanzados llegarán al listado completo. A su vez, se marcará la página del listado completo con un `no-index` para que Google no pierda tiempo rastreándolo. No es necesario añadir el `no-index` en la maquetación.

#### Principal
Por el momento no está definido como se mostrarán los resultados del buscador por lo que no es necesario programar la funcionalidad, solamente maqutar el diseño.

Los widgets de recientes y favoritos y los lobbies de juegos y jackpots serán iguales y se comportarán igual que los de la *Home*.

Al final hay un CTA hacia *Todas las slots*.

#### Todas las slots

Listado completo de slots, el grid y la informacion de cada ítem es igual que en los lobbies que hemos visto anteriormente.

En esta sección podremos filtrar el listado por nombre, novedad, promoción, favoritos, proveedor, tipo, etc.

Los filtros dentro de una misma categoría de filtros serán inclusivos (por ejemplo, proveedores NetEnt O Playtech) y los filtros de diferente categoría, exclusivos (por ejemplo, proveedor NetEnt Y apuesta mínima 0,10€).

Los resultados podran ordenarse y se podrán resetear los filtros. Deberá mostrarse en todo momento el numero de ítems actualizado.

Obviar por ahora el botón *Vista PRO*.

### Página de juego
**Páginas 49, 51, 54 y 55**
Esta página será compartida por juegos de tipo slot, casino, apuestas virtuales y ruleta en vivo. Sólo hay que maquetarla una vez, intentado incluir todos los widgets de los diferentes diseños.

Los juegos cargan dentro de iframes en vista desktop. En vista mobile se abren en una ventana nueva tras pulsar en el botón *¡Juega!*. Simular el juego cargando dentro de un iframe cualquier contenido.

En la vista desktop tenemos una serie de iconos que corresponden a las siguientes funcionalidades: Añadir a favoritos, abrir en ventana nueva, activar pantalla completa, desactivar pantalla completa, cerrar (vuelve al listado de juegos), esconder barra lateral.

Al pulsar sobre pantalla completa, el elemento a ampliar no es el iframe sinó el `div` que contiene la barra con los iconos, el juego y la barra lateral (que se esconderá automáticamente).

Este mismo contenedor, junto al header, debe ocupar justo el alto del viewport.

## Casino
**Páginas 52 y 53**
Lobby común con filtros simplificados: Tipo, proveedor y favoritos.

## Ruleta en vivo
**Página 60 y 61**
Lobby común con filtros simplificados: Tipo, proveedor y favoritos.

Este lobby muestra más información. Esta información es dinámica, cambia en tiempo real pero esta funcionalidad no es necesario aplicarla en la fase de maquetación. Se implementará cuando se migre el código a la plataforma. 

## Promociones 
**Páginas 56 y 57**
Listado de promociones filtrables por categoría. La categoría de cada promoción se distingue por un icono en la parte superior derecha de la caja y por un subtítulo bajo el título.

## Página de promoción
**Páginas 58 y 59**
Sección compuesta por una imagen destacada, el titulo, las caracteristicas (maquetar solamente las características del ejemplo), el texto del cuerpo, un call to action y un segundo texto.

El call to action tendrá 2 estados, activo e inactivo: *¡Apúntate!* y *Ya estás apuntado*.

## Poker
En proceso de diseño. Son secciones bastante estáticas, como mucho tienen desplegables tipo FAQ.

## Apuestas deportivas
En proceso de diseño. Lobby como la sección *Casino*.