import LobbySection from '../components/lobby-section';
import JackpotSection from '../components/jackpot-section';
import JackpotCounter from '../components/jackpot-counter';

window.JackpotCounter = JackpotCounter;

const DOMLoaded = () => {
    LobbySection();
    JackpotSection();
};

document.addEventListener('DOMContentLoaded', DOMLoaded);