import ScrollShadows from './scroll-shadows';

const lobbies = document.querySelectorAll('.jackpot-section');

const JackpotSection = () => {
    run();
}

const run = () => {
    handleShadows();
};

const handleShadows = () => {
    lobbies.forEach(lobby => {
        const scrollShadows = new ScrollShadows({
            element: lobby,
            shadowWrapper: '.jackpot-section__shadow-wrapper',
            listWrapper: '.jackpot-section__items-wrapper'
        });
    });   
};

export default JackpotSection;