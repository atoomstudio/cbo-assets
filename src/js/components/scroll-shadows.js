class ScrollShadows {
    defaultConfig = {
        element: null,
        shadowWrapper: '',
        listWrapper: ''
    };
    config = {};
    shadowWrapperEl = null;
    listWrapperEl = null;


    constructor(config = {}) {
        this.config = Object.assign({}, this.defaultConfig, config);
        this.setDomElements();
        this.addClasses();
        this.handleScroll();
        this.addShadowElements();
    }

    setDomElements = () => {
        this.shadowWrapperEl = this.config.element.querySelector(this.config.shadowWrapper);
        this.listWrapperEl = this.config.element.querySelector(this.config.listWrapper);
    };

    addShadowElements = () => {
        const shadow = document.createElement("div");
        shadow.classList.add(["scroll-shadow__element"])

        const shadowRight = shadow.cloneNode();
        shadowRight.classList.add("scroll-shadow__element--right");

        const shadowLeft = shadow.cloneNode();
        shadowLeft.classList.add("scroll-shadow__element--left");
        
        this.shadowWrapperEl.appendChild(shadowRight);
        this.shadowWrapperEl.appendChild(shadowLeft);
    }

    addClasses = () => {
        this.shadowWrapperEl.classList.add('scroll-shadow__wrapper');
    };

    handleScroll = () => {
        const maxScroll = this.listWrapperEl.scrollWidth - this.listWrapperEl.offsetWidth;
        this.listWrapperEl.addEventListener('scroll', (e) => {
            if (this.listWrapperEl.scrollLeft === 0) {
                this.shadowWrapperEl.dataset.scrollable = 'right';
            } else if (this.listWrapperEl.scrollLeft === maxScroll) {
                this.shadowWrapperEl.dataset.scrollable = 'left';
            } else {
                this.shadowWrapperEl.dataset.scrollable = 'both';
            }
        });
    };
}

export default ScrollShadows;