import ScrollShadows from './scroll-shadows';

const lobbies = document.querySelectorAll('.lobby-section');

const LobbySection = () => {
    run();
}

const run = () => {
    handleShadows();
};

const handleShadows = () => {
    lobbies.forEach(lobby => {
        const scrollShadows = new ScrollShadows({
            element: lobby,
            shadowWrapper: '.lobby-section__shadow-wrapper',
            listWrapper: '.lobby-section__items-wrapper'
        });
    });   
};

export default LobbySection;