const JackpotCounter = (element, amount = 0) => {
    if (!element) return;

    const formatter = new Intl.NumberFormat('es-ES', {
        style: 'currency',
        currency: 'EUR'
    });

    element.innerText = formatter.format(amount);
    setInterval(() => {
        amount += Math.random()/10;
        element.innerText = formatter.format(amount);
    }, 50);
}

export default JackpotCounter;